#!/usr/bin/python
# sbi - 1000rs

class Account:  # parent class
	def __init__(self):    # speical method - constructor
		self.balance = 1000
	def my_deposit(self,amount):
		self.balance = self.balance + amount
		return self.balance
	def my_withdraw(self,amount):
		self.balance = self.balance - amount
		return self.balance
	def my_balance(self):
		return "my balance is {}".format(self.balance)

class MinBalanceAccount(Account): # child class
	def __init__(self):
		Account.__init__(self)
	def my_withdraw(self,amount):
		if self.balance - amount < 1000:
			print "time to call  your daddy"
		else:
			Account.my_withdraw(self,amount)


# satyan - employee
satyan = Account() # __init__ get called implicitly.
print satyan.my_balance()# 1000
satyan.my_deposit(5000) 
print satyan.my_balance()# 6000
satyan.my_withdraw(3000)
print satyan.my_balance() # 3000
satyan.my_withdraw(6000)
print satyan.my_balance()

# madhav - student
madhav = MinBalanceAccount()
print madhav.my_balance()
madhav.my_deposit(5000) 
print madhav.my_balance()
madhav.my_withdraw(3000)
print madhav.my_balance()
madhav.my_withdraw(3000)
madhav.my_withdraw(1500)
print madhav.my_balance()
