#!/usr/bin/python

class Account:
	def __init__(self):    # speical method - constructor
		self.balance = 1000
	def my_deposit(self):
		self.balance = self.balance + 1000
		return self.balance
	def my_withdraw(self):
		self.balance = self.balance - 200
		return self.balance
	def my_balance(self):
		return "my balance is {}".format(self.balance)

# Acccount class
kumar = Account
print kumar,type(kumar)
print dir(kumar)

# satyan
satyan = Account() # __init__ get called implicitly.
print dir(satyan)
print satyan.my_balance()
satyan.my_deposit()
print satyan.my_balance()
satyan.my_withdraw()
print satyan.my_balance()

# madhav
madhav = Account()
print madhav.my_balance()
madhav.my_withdraw()
print madhav.my_balance()

print satyan.my_balance()
print madhav.my_balance()
