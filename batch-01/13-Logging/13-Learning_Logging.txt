# Logging
event handling
irctc 
- tatkal - 10:00am - 11:00am

java - log4j
python - logging

Log levels: functions
logging.DEBUG: logging.debug()

more granular messages
# ssh localhost
# ssh -v localhost
# ssh -vv localhost
# ssh -vvv localhost

logging.INFO : logging.info()

ex:
khyaathi@khyaathi-Technologies:~$ tail /var/log/syslog
Oct 12 06:07:24 khyaathi-Technologies sublime_text[14871]: Source ID 147672 was not found when attempting to remove it
Oct 12 06:07:25 khyaathi-Technologies sublime_text[14871]: Source ID 148048 was not found when attempting to remove it
Oct 12 06:07:32 khyaathi-Technologies sublime_text[14871]: Source ID 150760 was not found when attempting to remove it

logging.WARNING: logging.warning()

disk - 75% 
warning is a event to tell about a impending error.

logging.ERROR: logging.error()

Raid controlled disk - array of disk - 3 disks
one of the disk goes bad.
error is a state where we have a challenge but the application is still not affected.

logging.CRITICAL: logging.critical()

share marketing.

webpage -> shares,cost 
cost column goes blank.


# levels
DEBUG < INFO < WARNING < ERROR < CRITICAL

khyaathi@khyaathi-Technologies:~/Documents/bit-tuxfux/tuxfux.bitbucket.io/others/batch-01/13-Logging$ python fourth.py 
khyaathi@khyaathi-Technologies:~/Documents/bit-tuxfux/tuxfux.bitbucket.io/others/batch-01/13-Logging$ df -h /
Filesystem      Size  Used Avail Use% Mounted on
/dev/sda1        28G   13G   14G  49% /
khyaathi@khyaathi-Technologies:~/Documents/bit-tuxfux/tuxfux.bitbucket.io/others/batch-01/13-Logging$ dd if=/dev/zero of=bigfile count=1024 bs=1M
1024+0 records in
1024+0 records out
1073741824 bytes (1.1 GB, 1.0 GiB) copied, 6.50693 s, 165 MB/s
khyaathi@khyaathi-Technologies:~/Documents/bit-tuxfux/tuxfux.bitbucket.io/others/batch-01/13-Logging$ df -h /
Filesystem      Size  Used Avail Use% Mounted on
/dev/sda1        28G   14G   13G  53% /
khyaathi@khyaathi-Technologies:~/Documents/bit-tuxfux/tuxfux.bitbucket.io/others/batch-01/13-Logging$ ls -l
total 1048608
-rw-r--r-- 1 khyaathi khyaathi       1248 Oct 12 06:18 13-Learning_Logging.txt
-rw-r--r-- 1 khyaathi khyaathi 1073741824 Oct 12 06:57 bigfile
-rw-r--r-- 1 khyaathi khyaathi        465 Oct 12 06:56 disk_usage.log
-rw-r--r-- 1 khyaathi khyaathi        549 Oct 12 06:20 first.py
-rw-r--r-- 1 khyaathi khyaathi        940 Oct 12 06:56 fourth.py
-rw-r--r-- 1 khyaathi khyaathi       1338 Oct 12 06:32 log.txt
-rw-r--r-- 1 khyaathi khyaathi        462 Oct 12 06:34 second.py
-rw-r--r-- 1 khyaathi khyaathi        687 Oct 12 06:40 third.py
khyaathi@khyaathi-Technologies:~/Documents/bit-tuxfux/tuxfux.bitbucket.io/others/batch-01/13-Logging$ python fourth.py 
khyaathi@khyaathi-Technologies:~/Documents/bit-tuxfux/tuxfux.bitbucket.io/others/batch-01/13-Logging$ rm -f bigfile 
khyaathi@khyaathi-Technologies:~/Documents/bit-tuxfux/tuxfux.bitbucket.io/others/batch-01/13-Logging$ python fourth.py 
khyaathi@khyaathi-Technologies:~/Documents/bit-tuxfux/tuxfux.bitbucket.io/others/batch-01/13-Logging$ 

In [8]: import logging as l

In [9]: l.
l.BASIC_FORMAT        l.Handler             l.StreamHandler       l.critical            l.info                l.shutdown
l.BufferingFormatter  l.INFO                l.WARN                l.currentframe        l.log                 l.sys
l.CRITICAL            l.LogRecord           l.WARNING             l.debug               l.logMultiprocessing  l.thread
l.DEBUG               l.Logger              l.addLevelName        l.disable             l.logProcesses        l.threading
l.ERROR               l.LoggerAdapter       l.atexit              l.error               l.logThreads          l.time
l.FATAL               l.Manager             l.basicConfig         l.exception           l.makeLogRecord       l.traceback
l.FileHandler         l.NOTSET              l.cStringIO           l.fatal               l.os                  l.warn
l.Filter              l.NullHandler         l.captureWarnings     l.getLevelName        l.raiseExceptions     l.warning
l.Filterer            l.PlaceHolder         l.codecs              l.getLogger           l.root                l.warnings
l.Formatter           l.RootLogger          l.collections         l.getLoggerClass      l.setLoggerClass      l.weakref

In [9]: # l.StreamHandler,l.FileHandler

In [10]: import logging.handlers as lh

In [11]: lh.
lh.BaseRotatingHandler        lh.MemoryHandler              lh.SYSLOG_UDP_PORT            lh.logging
lh.BufferingHandler           lh.NTEventLogHandler          lh.SocketHandler              lh.os
lh.DEFAULT_HTTP_LOGGING_PORT  lh.RotatingFileHandler        lh.SysLogHandler              lh.re
lh.DEFAULT_SOAP_LOGGING_PORT  lh.SMTPHandler                lh.TimedRotatingFileHandler   lh.socket
lh.DEFAULT_TCP_LOGGING_PORT   lh.ST_DEV                     lh.WatchedFileHandler         lh.struct
lh.DEFAULT_UDP_LOGGING_PORT   lh.ST_INO                     lh.cPickle                    lh.time
lh.DatagramHandler            lh.ST_MTIME                   lh.codecs                     
lh.HTTPHandler                lh.SYSLOG_TCP_PORT            lh.errno                      

In [11]: lh.






reference:
https://docs.python.org/2/howto/logging.html
https://docs.python.org/2/library/logging.html
https://docs.python.org/2/howto/logging-cookbook.html