#!/usr/bin/python
# <type 'classobj'> -> synonymous to class.

class Account:
	balance = 1000  # class variable.



print Account,type(Account) 		# __main__.Account,<type 'classobj'>
print Account(),type(Account())		# <__main__.Account instance at 0x7f1c44ee23f8>,<type 'instance'>
print dir(Account)

# satyan
satyan = Account()
print satyan,type(satyan)
print dir(satyan)			# ['__doc__', '__module__', 'balance']
print satyan.balance        # instance variable
satyan.balance = 2000
print satyan.balance

# Madhav
madhav = Account()
print "madhav - {}".format(madhav.balance)        # instance variable


# class variable
print Account.balance # 1000
Account.balance = 5000
print Account.balance
print "satyan - {}".format(satyan.balance)
print "madhav - {}".format(madhav.balance)

