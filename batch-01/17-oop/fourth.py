#!/usr/bin/python

class Account:
	balance = 1000  # class variable. ex:Account.balance
	def my_balance(self): # method
		return "my balance is {}".format(self.balance)

# satyan
satyan = Account()
print satyan,type(satyan)
print satyan.balance
print satyan.my_balance() # understand the error below
'''
Traceback (most recent call last):
  File "fourth.py", line 12, in <module>
    print satyan.my_balance()
TypeError: my_balance() takes no arguments (1 given)
'''
# After adding the self to my_balance(self) in class defination
'''
Traceback (most recent call last):
  File "fourth.py", line 12, in <module>
    print satyan.my_balance() # understand the error below
  File "fourth.py", line 6, in my_balance
    print "my balance is {}".format(balance)
NameError: global name 'balance' is not defined
'''
# to correct this lets make balance as satyan.balance

# Madhav
madhav = Account()
madhav.balance = 3000
print madhav.my_balance() # 1000
# Madhav is still show as 1000 - we have satyan.balance in my_balance(self).
# to recifiy it i have made a slight change of self.balance in line 6.
