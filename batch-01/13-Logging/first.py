#!/usr/bin/python

import logging

logging.debug("This is a debug message.")
logging.info("This is a information message.")
logging.warning("This is a warning message.")
logging.error("This is an error message.")
logging.critical("This is a critical message")


'''
WARNING:root:This is a warning message.
ERROR:root:This is an error message.
CRITICAL:root:This is a critical message

The default level is WARNING, which means that only events of this level and above will be tracked, 
unless the logging package is configured to do otherwise.


'''