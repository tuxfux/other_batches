#!/usr/bin/python

version=2.0

def my_add(a,b):
	''' This function is for addition of two numbers '''
	return a + b

def my_sub(a,b):
	''' This function is for substraction of two numbers '''
	if a > b:
		return a - b
	else:
		return b - a

def my_div(a,b):
	''' This is for the division of two numbers '''
	return a/b

def my_multi(a,b):
	''' This is for the multiplication of two numbers '''
	return a * b

## MAIN

# main(void)
if __name__ == '__main__':
	print "we are launching a missile"