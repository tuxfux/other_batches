#!/usr/bin/python

import logging

# logging.basicConfig(filename='disk_usage.log',filemode='a',level=logging.DEBUG,
# 					format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
# 					datefmt='%c')

'''
# Loggers expose the interface that application code directly uses.
# ex:logging module: root
# Handlers send the log records (created by loggers) to the appropriate destination.
# ex: filename='disk_usage.log',filemode='a'
# Filters provide a finer grained facility for determining which log records to output.
# ex:level=logging.DEBUG
# Formatters specify the layout of log records in the final output.
# ex: format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',datefmt='%c'
'''
# create logger
logger = logging.getLogger('simple_example') # logger is a variables.
logger.setLevel(logging.DEBUG)               # filter at logger level.

# create console handler and set level to debug
ch = logging.StreamHandler() # handler
ch.setLevel(logging.DEBUG)   # filter at logging level.

# create formatter
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s') # layout

# add formatter to ch
ch.setFormatter(formatter) # handler and formatter.

# add ch to logger
logger.addHandler(ch) # logger and handler.

# 'application' code
logger.debug('debug message')
logger.info('info message')
logger.warn('warn message')
logger.error('error message')
logger.critical('critical message')



'''
Improvements:
* I cannot modify the logger name.
* we cannot redirect all logs to the same file.
* we cannot use other handlers and we are restriced to only file handler.

'''