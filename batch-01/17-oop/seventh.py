#!/usr/bin/python
# user defined exception
# Exception ia parent class
# InvalidAgeException being the parent class.

class InvalidAgeException(Exception): # child class
	def __init__(self,age):
		self.age = age

def validate_age(age):
	if age > 18:
		return "welcome to the movie."
	else:
		raise InvalidAgeException(age)



age = int(raw_input("please enter your age=>"))
try:
	validate_age(age)
except Exception as e:
	print "buddy you are still a kid = {}".format(e.age)
else:
	print validate_age(age)
finally:
	pass
