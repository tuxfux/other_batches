In [1]: # we dont need to typecast our variables.

In [2]: my_string="python"

In [3]: my_string.
my_string.capitalize  my_string.find        my_string.isspace     my_string.partition   my_string.rstrip      my_string.translate
my_string.center      my_string.format      my_string.istitle     my_string.replace     my_string.split       my_string.upper
my_string.count       my_string.index       my_string.isupper     my_string.rfind       my_string.splitlines  my_string.zfill
my_string.decode      my_string.isalnum     my_string.join        my_string.rindex      my_string.startswith  
my_string.encode      my_string.isalpha     my_string.ljust       my_string.rjust       my_string.strip       
my_string.endswith    my_string.isdigit     my_string.lower       my_string.rpartition  my_string.swapcase    
my_string.expandtabs  my_string.islower     my_string.lstrip      my_string.rsplit      my_string.title       

In [3]: my_string.title?
Type:        builtin_function_or_method
String form: <built-in method title of str object at 0x7fe6b2dd30f0>
Docstring:
S.title() -> string

Return a titlecased version of S, i.e. words start with uppercase
characters, all remaining cased characters have lowercase.

In [4]: my_string.title()
Out[4]: 'Python'

In [5]: print my_string,type(my_string)
python <type 'str'>

In [7]: # string is a sequnce of characters

In [8]: print my_string
python

In [9]: # p  y  t  h  o  n

In [10]: # p  y  t  h  o  n

In [11]: # 0  1  2  3  4  5 # +ve indexing or left to right

In [12]: # -6 -5 -4 -3 -2 -1 # -ve indexing or right to left

In [13]: my_string[3]
Out[13]: 'h'

In [14]: my_string[-3]
Out[14]: 'h'

In [15]: my_string[-1]
Out[15]: 'n'

In [16]: # you cannot overwrite the index values

In [17]: my_string[-1]
Out[17]: 'n'

In [18]: my_string[-1]='N'
---------------------------------------------------------------------------
TypeError                                 Traceback (most recent call last)
<ipython-input-18-09cf5bc1c9bc> in <module>()
----> 1 my_string[-1]='N'

TypeError: 'str' object does not support item assignment

In [19]: # i cannot modify the elements of a string,but can overwrite the whole string.

In [21]: # p  y  t  h  o  n

In [22]: # 0  1  2  3  4  5 # +ve indexing or left to right

In [23]: # -6 -5 -4 -3 -2 -1 # -ve indexing or right to left

In [24]: ## slicing

In [25]: my_string[0:3] # start from 0 till 3 (3 not included)
Out[25]: 'pyt'

In [26]: my_string[:3] # start from 0 till 3 (3 not included)
Out[26]: 'pyt'

In [27]: my_string[3:6]
Out[27]: 'hon'

In [28]: my_string[3:9]
Out[28]: 'hon'

In [29]: my_string[3:]
Out[29]: 'hon'

In [30]: my_string[0:6]
Out[30]: 'python'

In [31]: my_string[:]
Out[31]: 'python'

In [32]: ## step

In [33]: my_string[::1]
Out[33]: 'python'

In [34]: my_string[0:6:2]
Out[34]: 'pto'

In [35]: my_string[1:6:2]
Out[35]: 'yhn'

In [37]: # p  y  t  h  o  n

In [38]: # -6 -5 -4 -3 -2 -1 # -ve indexing or right to left

In [39]: # 0  1  2  3  4  5 # +ve indexing or left to right

In [40]: my_string[6:0:-1]
Out[40]: 'nohty'

In [41]: my_string[::-1]
Out[41]: 'nohtyp'

In [42]: my_string[-6:-3]
Out[42]: 'pyt'

In [43]: ## concatination

In [44]: my_string[3:]
Out[44]: 'hon'

In [45]: my_string[:3]
Out[45]: 'pyt'

In [46]: my_string[3:] + my_string[:3]
Out[46]: 'honpyt'

In [47]: my_string[:3] + my_string[3:]
Out[47]: 'python'

In [48]: ## multiply string

In [49]: '-' * 10
Out[49]: '----------'

In [50]: ## task1

In [51]: my_string
Out[51]: 'python'

In [52]: # output = Tython

In [53]: 'T' + my_string[1:]
Out[53]: 'Tython'

In [54]: my_string[2].upper() + my_string[1:]
Out[54]: 'Tython'

In [55]: my_string.replace?
Type:        builtin_function_or_method
String form: <built-in method replace of str object at 0x7fe6b2dd30f0>
Docstring:
S.replace(old, new[, count]) -> string

Return a copy of string S with all occurrences of substring
old replaced by new.  If the optional argument count is
given, only the first count occurrences are replaced.

In [56]: my_string.replace('p','T')
Out[56]: 'Tython'

In [57]: my_string[0]="T'
  File "<ipython-input-57-197a4d2a3ca6>", line 1
    my_string[0]="T'
                   ^
SyntaxError: EOL while scanning string literal


In [58]: my_string[0]="T"
---------------------------------------------------------------------------
TypeError                                 Traceback (most recent call last)
<ipython-input-58-17f39fee6056> in <module>()
----> 1 my_string[0]="T"

TypeError: 'str' object does not support item assignment

In [60]: ## playing with numbers

In [61]: 1 + 1
Out[61]: 2

In [62]: '1' + '1'
Out[62]: '11'

In [63]: 2 * 2
Out[63]: 4

In [64]: 25 / 2 
Out[64]: 12

In [65]: 25.0 / 2
Out[65]: 12.5

In [66]: 25 / 2.0
Out[66]: 12.5

In [67]: # typecasting functions - float,int,str

In [68]: float(25)
Out[68]: 25.0

In [69]: float(25)/2
Out[69]: 12.5

In [70]: 25 %  2
Out[70]: 1

In [71]: 25 + 25 / 2
Out[71]: 37

In [72]: (25 + 25)/2
Out[72]: 25

In [73]: #BODMAS

In [75]: ## printing

In [76]: my_school="De Paul"

In [77]: another_school="st. xaviers"

In [78]: town="township"

In [79]: beach="blue"

In [80]: commute="bus"

In [81]: print "my school name is my_school"
my school name is my_school

In [82]: print 'my school name is my_school'
my school name is my_school

In [83]: print "my school name is", my_school
my school name is De Paul

In [84]: # case I

In [85]: print "my school name is", my_school, "we had a school adjacent to ours", another_school , "our town is a small", town, 'we had beach which has color', beach, "we used to commute on", commute
my school name is De Paul we had a school adjacent to ours st. xaviers our town is a small township we had beach which has color blue we used to commute on bus

In [86]: #case II - typecasting - %s,%d,%f,%r

In [87]: print "my school name is %s.we had a school adjacent to ours %s.our town is a small %s. we had a beach which has color %s. We used to commute on %s" %(my_school,another_school,town,beach,commute)
my school name is De Paul.we had a school adjacent to ours st. xaviers.our town is a small township. we had a beach which has color blue. We used to commute on bus

In [88]: print "my school name is %s.I love my %s." %(my_school,my_school)
my school name is De Paul.I love my De Paul.

In [89]: print "my school name is %s.I love my %s." %(my_school,another_school)
my school name is De Paul.I love my st. xaviers.

In [91]: # case III - format

In [92]: print "my school name is %s.I love my %s." %(my_school,my_school)
my school name is De Paul.I love my De Paul.

In [93]: print "my school name is {}.I love my {}".format(my_school,my_school)
my school name is De Paul.I love my De Paul

In [94]: print "my school name is {0}.I love my {0}".format(my_school,another_school)
my school name is De Paul.I love my De Paul

In [95]: print "my school name is {1}.I love my {1}".format(my_school,another_school)
my school name is st. xaviers.I love my st. xaviers

In [96]: print "my school name is {ms}.I love my {ms}".format(ms=my_school,ans=another_school)
my school name is De Paul.I love my De Paul

In [98]: # input

In [99]: # raw_input(2.x) , input(2.x and 3.x)

In [100]: name = raw_input("please enter your name:")
please enter your name:kumar

In [101]: print name,type(name)
kumar <type 'str'>

In [102]: num = raw_input("please enter a number:")
please enter a number:10

In [103]: print num,type(num)
10 <type 'str'>

In [104]: # typecasting functions

In [105]: num = int(raw_input("please enter a number:"))
please enter a number:10

In [106]: print num
10

In [107]: print num,type(num)
10 <type 'int'>

In [108]: num = int(raw_input("please enter a number:"))
please enter a number:ten
---------------------------------------------------------------------------
ValueError                                Traceback (most recent call last)
<ipython-input-108-edb42c6cf795> in <module>()
----> 1 num = int(raw_input("please enter a number:"))

ValueError: invalid literal for int() with base 10: 'ten'

In [109]: 

In [110]: # input

In [111]: num = int(raw_input("please enter a number:"))
please enter a number:10

In [112]: print num,type(num)
10 <type 'int'>

In [113]: num = input("please enter your number:")
please enter your number:110

In [114]: print num,type(num)
110 <type 'int'>

In [115]: num = input("please enter your number:")
please enter your number:11.0

In [116]: print num,type(num)
11.0 <type 'float'>

In [117]: 
