#!/usr/bin/python

def account():
	return {'balance':0}

def deposit(user):
	user['balance'] = user['balance'] + 1000
	return user['balance']

def withdraw(user):
	user['balance'] = user['balance'] - 200
	return user['balance']

def balance(user):
	return "balance for the user - {}".format(user)


# satyan
satyan = account()
print satyan
print balance(satyan)
deposit(satyan)
print balance(satyan)
withdraw(satyan)
print balance(satyan)

# Madhav
madhav = account()
print madhav

