#!/usr/bin/python
# logging.basicConfig
# logging.Formatter
# google.com -> man date

import logging

logging.basicConfig(filename='disk_usage.log',filemode='a',level=logging.DEBUG,
					format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
					datefmt='%c')

disk_usage = int(raw_input("please enter the disk value:"))

if disk_usage < 50: 
	logging.info("The disk is going health at - {}.".format(disk_usage))
elif disk_usage < 75:
	logging.warning("Your disk is getting filled up - {}.".format(disk_usage))
elif disk_usage < 90:
	logging.error("Your disk has started to puke errors - {}".format(disk_usage))
elif disk_usage < 99:
	logging.critical("your applicaiton is down - {}".format(disk_usage))