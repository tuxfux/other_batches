#!/usr/bin/python
import MySQLdb as mdb
conn = mdb.connect('localhost','user01','user01','batch01')
#conn1 = mdb.connect('server details','user details','password details','schema')
cur = conn.cursor()
cur.execute('create table student(name varchar(10),gender varchar(6))')
conn.close()

'''
mysql> show tables;
+-------------------+
| Tables_in_batch01 |
+-------------------+
| student           |
+-------------------+
1 row in set (0.00 sec)

mysql> desc student;
+--------+-------------+------+-----+---------+-------+
| Field  | Type        | Null | Key | Default | Extra |
+--------+-------------+------+-----+---------+-------+
| name   | varchar(10) | YES  |     | NULL    |       |
| gender | varchar(6)  | YES  |     | NULL    |       |
+--------+-------------+------+-----+---------+-------+
2 rows in set (0.07 sec)

mysql> 
'''