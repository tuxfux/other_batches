lists - linear representation of data.
lists can save hetrogenous data.

my_fruits1 = 'apple'
my_fruits2 = 'banana'

lists vs arrays
arrays are multi dimension representation of data.

# https://www.scipy.org/

In [5]: my_fruits = ["apple","banana","cherry","dates"]

In [6]: print  my_fruits,type(my_fruits)
['apple', 'banana', 'cherry', 'dates'] <type 'list'>

In [7]: my_empty = []

In [8]: print my_empty,type(my_empty)
[] <type 'list'>

In [9]: my_empty = list()

In [10]: print my_empty,type(my_empty)
[] <type 'list'>

In [11]: # lists - indexing and slicking

In [12]: # ["apple","banana","cherry","dates"]

In [13]: #   0        1         2       3

In [14]: #   -4       -3        -2      -1

In [15]: my_fruits[0]
Out[15]: 'apple'

In [16]: my_fruits[-4]
Out[16]: 'apple'

In [17]: my_fruits[2]
Out[17]: 'cherry'

In [18]: # slicing

In [19]: my_fruits[1:3]
Out[19]: ['banana', 'cherry']

In [20]: 

In [20]: # in operator

In [21]: 'apple' in my_fruits
Out[21]: True

In [22]: 'dates' in my_fruits
Out[22]: True

--- day02 lists ----

In [2]: my_string="python"

In [3]: my_fruits = ["apple","banana","cherry","dates"]

In [4]: my_fruits[0]='Apple'

In [5]: print my_fruits
['Apple', 'banana', 'cherry', 'dates']

In [6]: my_string[0]
Out[6]: 'p'

In [7]: my_string[0]='T'
---------------------------------------------------------------------------
TypeError                                 Traceback (most recent call last)
<ipython-input-7-a6e1b6047af7> in <module>()
----> 1 my_string[0]='T'

TypeError: 'str' object does not support item assignment

In [8]: 

 # converting a list to string and vice-versa

 In [9]: # converting a list to string and vice-versa

In [10]: my_string="python"

In [11]: # covert string to list

In [12]: Lmy_string = list(my_string)

In [13]: print Lmy_string
['p', 'y', 't', 'h', 'o', 'n']

In [14]: Lmy_string[0]
Out[14]: 'p'

In [15]: Lmy_string[0]='T'

In [16]: print Lmy_string
['T', 'y', 't', 'h', 'o', 'n']

In [17]: # convert the list to a string

In [18]: limiter=""

In [19]: limiter.join(Lmy_string)
Out[19]: 'Tython'

In [20]: limiter.join?
Type:        builtin_function_or_method
String form: <built-in method join of str object at 0x7fde2d814508>
Docstring:
S.join(iterable) -> string

Return a string which is the concatenation of the strings in the
iterable.  The separator between elements is S.

In [21]: limiter=':'

In [22]: limiter.join(Lmy_string)
Out[22]: 'T:y:t:h:o:n'

In [24]: # convert a string to a list

In [25]: my_sentence="today is wednesday"

In [26]: my_sentence[0]="tomorrow"
---------------------------------------------------------------------------
TypeError                                 Traceback (most recent call last)
<ipython-input-26-146d6fef15b1> in <module>()
----> 1 my_sentence[0]="tomorrow"

TypeError: 'str' object does not support item assignment

In [27]: Lmy_sentence=list(my_sentence)

In [28]: print Lmy_sentence
['t', 'o', 'd', 'a', 'y', ' ', 'i', 's', ' ', 'w', 'e', 'd', 'n', 'e', 's', 'd', 'a', 'y']

In [29]: my_sentence.split?
Type:        builtin_function_or_method
String form: <built-in method split of str object at 0x7fde2922e5e0>
Docstring:
S.split([sep [,maxsplit]]) -> list of strings

Return a list of the words in the string S, using sep as the
delimiter string.  If maxsplit is given, at most maxsplit
splits are done. If sep is not specified or is None, any
whitespace string is a separator and empty strings are removed
from the result.

In [30]: Lmy_sentence=my_sentence.split()

In [31]: print Lmy_sentence
['today', 'is', 'wednesday']

In [32]: Lmy_sentence[0]
Out[32]: 'today'

In [33]: Lmy_sentence[0]="Tomorrow"

In [34]: print Lmy_sentence
['Tomorrow', 'is', 'wednesday']

In [35]: # list back to a string

In [36]: delimiter=" "

In [37]: delimiter.join?
Type:        builtin_function_or_method
String form: <built-in method join of str object at 0x7fde2d814e90>
Docstring:
S.join(iterable) -> string

Return a string which is the concatenation of the strings in the
iterable.  The separator between elements is S.

In [38]: delimiter.join(Lmy_sentence)
Out[38]: 'Tomorrow is wednesday'

In [39]: new_string=delimiter.join(Lmy_sentence)

In [40]: print new_string
Tomorrow is wednesday

In [41]: print my_string
python

In [42]: print my_sentence
today is wednesday

In [44]: # functions

In [45]: print my_fruits
['Apple', 'banana', 'cherry', 'dates']

In [46]: my_fruits.
my_fruits.append   my_fruits.extend   my_fruits.insert   my_fruits.remove   my_fruits.sort     
my_fruits.count    my_fruits.index    my_fruits.pop      my_fruits.reverse  

In [46]: my_fruits.append?
Type:        builtin_function_or_method
String form: <built-in method append of list object at 0x7fde29240170>
Docstring:   L.append(object) -- append object to end

In [47]: my_fruits.append('elderberry')

In [48]: print my_fruits
['Apple', 'banana', 'cherry', 'dates', 'elderberry']

In [49]: # my_fruits.extend

In [50]: my_fruits.extend?
Type:        builtin_function_or_method
String form: <built-in method extend of list object at 0x7fde29240170>
Docstring:   L.extend(iterable) -- extend list by appending elements from the iterable

In [51]: my_fruits.extend(['fig','guava','jackfruit'])

In [52]: print my_fruits
['Apple', 'banana', 'cherry', 'dates', 'elderberry', 'fig', 'guava', 'jackfruit']

In [53]: # my_fruits.insert

In [54]: my_fruits.insert?
Type:        builtin_function_or_method
String form: <built-in method insert of list object at 0x7fde29240170>
Docstring:   L.insert(index, object) -- insert object before index

In [55]: my_fruits.insert(-2,'grapes')

In [56]: print my_fruits
['Apple', 'banana', 'cherry', 'dates', 'elderberry', 'fig', 'grapes', 'guava', 'jackfruit']

In [57]: 

In [58]: my_fruits
Out[58]: 
['Apple',
 'banana',
 'cherry',
 'dates',
 'elderberry',
 'fig',
 'grapes',
 'guava',
 'jackfruit']

In [59]: #my_fruits.index

In [60]: my_fruits.index?
Type:        builtin_function_or_method
String form: <built-in method index of list object at 0x7fde29240170>
Docstring:
L.index(value, [start, [stop]]) -> integer -- return first index of value.
Raises ValueError if the value is not present.

In [61]: my_fruits.index('grapes')
Out[61]: 6

In [62]: my_fruits.index('Grapes')
---------------------------------------------------------------------------
ValueError                                Traceback (most recent call last)
<ipython-input-62-861b72687ab4> in <module>()
----> 1 my_fruits.index('Grapes')

ValueError: 'Grapes' is not in list

In [63]: 


In [64]: print my_fruits
['Apple', 'banana', 'cherry', 'dates', 'elderberry', 'fig', 'grapes', 'guava', 'jackfruit']

In [65]: my_fruits.append('grapes')

In [66]: print my_fruits
['Apple', 'banana', 'cherry', 'dates', 'elderberry', 'fig', 'grapes', 'guava', 'jackfruit', 'grapes']

In [67]: #my_fruits.count

In [68]: my_fruits.count?
Type:        builtin_function_or_method
String form: <built-in method count of list object at 0x7fde29240170>
Docstring:   L.count(value) -> integer -- return number of occurrences of value

In [69]: my_fruits.count('grapes')
Out[69]: 2

In [70]: my_fruits.count('guava')
Out[70]: 1

In [72]: my_fruits.
my_fruits.append   my_fruits.extend   my_fruits.insert   my_fruits.remove   my_fruits.sort     
my_fruits.count    my_fruits.index    my_fruits.pop      my_fruits.reverse  

In [72]: #my_fruits.sort

In [73]: my_fruits.sort?
Type:        builtin_function_or_method
String form: <built-in method sort of list object at 0x7fde29240170>
Docstring:
L.sort(cmp=None, key=None, reverse=False) -- stable sort *IN PLACE*;
cmp(x, y) -> -1, 0, 1

In [74]: print my_fruits
['Apple', 'banana', 'cherry', 'dates', 'elderberry', 'fig', 'grapes', 'guava', 'jackfruit', 'grapes']

In [75]: my_fruits.sort()

In [76]: print my_fruits
['Apple', 'banana', 'cherry', 'dates', 'elderberry', 'fig', 'grapes', 'grapes', 'guava', 'jackfruit']

In [77]: my_fruits.sort(reverse=True)

In [78]: print my_fruits
['jackfruit', 'guava', 'grapes', 'grapes', 'fig', 'elderberry', 'dates', 'cherry', 'banana', 'Apple']

In [79]: # my_fruits.reverse

In [80]: my_fruits.reverse?
Type:        builtin_function_or_method
String form: <built-in method reverse of list object at 0x7fde29240170>
Docstring:   L.reverse() -- reverse *IN PLACE*

In [81]: my_fruits.reverse()

In [82]: print my_fruits
['Apple', 'banana', 'cherry', 'dates', 'elderberry', 'fig', 'grapes', 'grapes', 'guava', 'jackfruit']

In [83]: 

In [84]: my_fruits.
my_fruits.append   my_fruits.extend   my_fruits.insert   my_fruits.remove   my_fruits.sort     
my_fruits.count    my_fruits.index    my_fruits.pop      my_fruits.reverse  

In [84]: my_fruits.pop?
Type:        builtin_function_or_method
String form: <built-in method pop of list object at 0x7fde29240170>
Docstring:
L.pop([index]) -> item -- remove and return item at index (default last).
Raises IndexError if list is empty or index is out of range.

In [85]: my_fruits
Out[85]: 
['Apple',
 'banana',
 'cherry',
 'dates',
 'elderberry',
 'fig',
 'grapes',
 'grapes',
 'guava',
 'jackfruit']

In [86]: my_fruits.index('grapes')
Out[86]: 6

In [87]: my_fruits.index?
Type:        builtin_function_or_method
String form: <built-in method index of list object at 0x7fde29240170>
Docstring:
L.index(value, [start, [stop]]) -> integer -- return first index of value.
Raises ValueError if the value is not present.

In [88]: my_fruits.index('grapes',7)
Out[88]: 7

In [89]: my_fruits.pop(7)
Out[89]: 'grapes'

In [90]: my_fruits.pop()
Out[90]: 'jackfruit'

In [91]: print my_fruits
['Apple', 'banana', 'cherry', 'dates', 'elderberry', 'fig', 'grapes', 'guava']

In [84]: my_fruits.
my_fruits.append   my_fruits.extend   my_fruits.insert   my_fruits.remove   my_fruits.sort     
my_fruits.count    my_fruits.index    my_fruits.pop      my_fruits.reverse  

In [84]: my_fruits.pop?
Type:        builtin_function_or_method
String form: <built-in method pop of list object at 0x7fde29240170>
Docstring:
L.pop([index]) -> item -- remove and return item at index (default last).
Raises IndexError if list is empty or index is out of range.

In [85]: my_fruits
Out[85]: 
['Apple',
 'banana',
 'cherry',
 'dates',
 'elderberry',
 'fig',
 'grapes',
 'grapes',
 'guava',
 'jackfruit']

In [86]: my_fruits.index('grapes')
Out[86]: 6

In [87]: my_fruits.index?
Type:        builtin_function_or_method
String form: <built-in method index of list object at 0x7fde29240170>
Docstring:
L.index(value, [start, [stop]]) -> integer -- return first index of value.
Raises ValueError if the value is not present.

In [88]: my_fruits.index('grapes',7)
Out[88]: 7

In [89]: my_fruits.pop(7)
Out[89]: 'grapes'

In [90]: my_fruits.pop()
Out[90]: 'jackfruit'

In [91]: print my_fruits
['Apple', 'banana', 'cherry', 'dates', 'elderberry', 'fig', 'grapes', 'guava']

In [92]: # my_fruits.remove

In [93]: my_fruits.remove?
Type:        builtin_function_or_method
String form: <built-in method remove of list object at 0x7fde29240170>
Docstring:
L.remove(value) -- remove first occurrence of value.
Raises ValueError if the value is not present.

In [94]: print my_fruits
['Apple', 'banana', 'cherry', 'dates', 'elderberry', 'fig', 'grapes', 'guava']

In [95]: my_fruits.remove('fig')

In [96]: print my_fruits
['Apple', 'banana', 'cherry', 'dates', 'elderberry', 'grapes', 'guava']

In [97]: my_fruits.remove('fig')
---------------------------------------------------------------------------
ValueError                                Traceback (most recent call last)
<ipython-input-97-ff64c0b5fdb4> in <module>()
----> 1 my_fruits.remove('fig')

ValueError: list.remove(x): x not in list

In [98]: 

## exercise - 1

In [99]: my_days = ['yesterday','today','tomorrow','dayafter']

In [100]: # output

In [101]: '''
   .....: yesterday 9
   .....: today     5
   .....: tomorrow  8
   .....: dayafter  9
   .....: '''
Out[101]: '\nyesterday 9\ntoday     5\ntomorrow  8\ndayafter  9\n'

In [102]: 

In [102]: for value in my_days:
   .....:     print value,len(value)
   .....:     
yesterday 9
today 5
tomorrow 8
dayafter 8

In [103]: len(my_days)
Out[103]: 4

## exercise - 2


In [105]: my_fruits = ['apple','apple','apple','banana','banana','cherry']

In [106]: #output

In [107]: # my_dupli = ['apple','banana']

In [108]: # my_fruits = ['apple','banana','cherry']

In [113]: for value in my_fruits:
   .....:     if my_fruits.count(value) > 1:
   .....:         if value not in my_dupli:
   .....:             my_dupli.append(value)
   .....:         my_fruits.remove(value)
   .....:         

In [114]: print my_dupli
['apple', 'banana']

In [115]: my_fruits
Out[115]: ['apple', 'banana', 'cherry']



In [2]: # soft and deep copy

In [3]: a=10

In [4]: print a
10

In [5]: id?
Type:        builtin_function_or_method
String form: <built-in function id>
Namespace:   Python builtin
Docstring:
id(object) -> integer

Return the identity of an object.  This is guaranteed to be unique among
simultaneously existing objects.  (Hint: it's the object's memory address.)

In [6]: id(a)
Out[6]: 94381926174848

In [7]: id(10)
Out[7]: 94381926174848

In [8]: b=10

In [9]: id(b)
Out[9]: 94381926174848

In [10]: ## is

In [11]: a is b
Out[11]: True

In [12]: c=a

In [13]: id(c)
Out[13]: 94381926174848

In [14]: # sharing a similar memory block on an object during assignment or during creation is called soft copy.

In [16]: print a
10

In [17]: print b
10

In [18]: print c
10

In [19]: b=20

In [20]: print a,b,c
10 20 10

In [21]: ## assigning a new memory block by not overwriting is called deep copy.

In [22]: id(a)
Out[22]: 94381926174848

In [23]: id(b)
Out[23]: 94381926174608

In [24]: id(c)
Out[24]: 94381926174848

In [25]: a is b
Out[25]: False

In [26]: a is c
Out[26]: True

In [27]: x="rose"

In [28]: id("rose")
Out[28]: 140009837489632

In [29]: id(x)
Out[29]: 140009837489632

In [30]: 5
Out[30]: 5

In [31]: id(5)
Out[31]: 94381926174968

###
# 
###

In [32]: # garbage collection

###
#  task 1
###
In [34]: x = 500

In [35]: y = 500

In [36]: id(x)
Out[36]: 94381930129792

In [37]: id(y)
Out[37]: 94381928248872

In [38]: x is y
Out[38]: False

###
#  task 2
###


In [85]: a=10

In [86]: b=a

In [87]: id(a),id(b)
Out[87]: (94381926174848, 94381926174848)

In [88]: c=10

In [89]: d=copy.deepcopy(c)

In [90]: id(c),id(d)
Out[90]: (94381926174848, 94381926174848)

In [91]: a is b
Out[91]: True

In [92]: c is d
Out[92]: True



In [39]: ## google -> why is this an odd case


In [41]: # memory allocation in lists

In [42]: a = [1,2,3]

In [43]: id(a)
Out[43]: 140009837641600

In [44]: id(a[0])
Out[44]: 94381926175064

In [45]: id(a[1])
Out[45]: 94381926175040

In [46]: id(a[2])
Out[46]: 94381926175016

In [47]: # soft copy

In [48]: b = a

In [49]: id(b)
Out[49]: 140009837641600

In [50]: id(b[0])
Out[50]: 94381926175064

In [51]: id(b[1])
Out[51]: 94381926175040

In [52]: id(b[2])
Out[52]: 94381926175016

In [53]: a is b
Out[53]: True

In [54]: a[0]
Out[54]: 1

In [55]: a[0]=10

In [56]: print a,b
[10, 2, 3] [10, 2, 3]

In [57]: a = [11,22,33]

In [58]: print a,b
[11, 22, 33] [10, 2, 3]

In [59]: print id(a),id(b)
140009844595744 140009837641600

In [60]: a=b

In [61]: a
Out[61]: [10, 2, 3]

In [62]: b
Out[62]: [10, 2, 3]

In [63]: a is b
Out[63]: True

In [64]: 

In [65]: # deep copy

In [66]: a=[11,22,33]

In [67]: import copy

In [68]: copy.
copy.Error           copy.copy            copy.dispatch_table  copy.name            copy.weakref         
copy.PyStringMap     copy.deepcopy        copy.error           copy.t               

In [68]: copy.deepcopy?
Type:        function
String form: <function deepcopy at 0x7f5697c3ea28>
File:        /usr/lib/python2.7/copy.py
Definition:  copy.deepcopy(x, memo=None, _nil=[])
Docstring:
Deep copy operation on arbitrary Python objects.

See the module's __doc__ string for more info.

In [69]: b = copy.deepcopy(a)

In [70]: id(b)
Out[70]: 140009854106440

In [71]: id(a)
Out[71]: 140009858831280

In [72]: print a,id(a),id(a[0]),id(a[1]),id(a[2])
[11, 22, 33] 140009858831280 94381926174824 94381926174560 94381926174296

In [73]: print b,id(b),id(b[0]),id(b[1]),id(b[2])
[11, 22, 33] 140009854106440 94381926174824 94381926174560 94381926174296

In [74]: a is b
Out[74]: False

In [75]: print a,b
[11, 22, 33] [11, 22, 33]

In [76]: a[0]="55"

In [77]: print a,b
['55', 22, 33] [11, 22, 33]

## list comprehensions

creating lists on fly.

In [2]: num = raw_input("please enter an number:")
please enter an number:1,2,3,4,5,6,7,8,9,10

In [3]: print num,type(num)
1,2,3,4,5,6,7,8,9,10 <type 'str'>

In [4]: # output => 2,4,6,8,10

In [5]: num.
num.capitalize  num.endswith    num.isalnum     num.istitle     num.lstrip      num.rjust       num.splitlines  num.translate
num.center      num.expandtabs  num.isalpha     num.isupper     num.partition   num.rpartition  num.startswith  num.upper
num.count       num.find        num.isdigit     num.join        num.replace     num.rsplit      num.strip       num.zfill
num.decode      num.format      num.islower     num.ljust       num.rfind       num.rstrip      num.swapcase    
num.encode      num.index       num.isspace     num.lower       num.rindex      num.split       num.title       

In [5]: num.split?
Type:        builtin_function_or_method
String form: <built-in method split of str object at 0x7f188f6f1330>
Docstring:
S.split([sep [,maxsplit]]) -> list of strings

Return a list of the words in the string S, using sep as the
delimiter string.  If maxsplit is given, at most maxsplit
splits are done. If sep is not specified or is None, any
whitespace string is a separator and empty strings are removed
from the result.

In [6]: num.split(',')
Out[6]: ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10']

In [7]: my_even=[]


In [9]: for value in num.split(','):
    if int(value) % 2 == 0:
        print value
        my_even.append(value)
   ...:         
2
4
6
8
10

In [10]: print my_even
['2', '4', '6', '8', '10']

In [11]: limiter=","

In [12]: limiter.join(my_even)
Out[12]: '2,4,6,8,10'

## example 2

In [19]: my_string="today is thursday"

In [20]: [value for value in my_string.split()]
Out[20]: ['today', 'is', 'thursday']

In [21]: [[value.capitalize(),value.lower(),value.upper()] for value in my_string.split()]
Out[21]: 
[['Today', 'today', 'TODAY'],
 ['Is', 'is', 'IS'],
 ['Thursday', 'thursday', 'THURSDAY']]

In [22]: 

# google: list comprehension









