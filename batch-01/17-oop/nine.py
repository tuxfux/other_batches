#!/usr/bin/python
# public,private,protected

# case I: public
# your class variables/methods are public by default.

# class Cup:
# 	color=None
# 	content=None
# 	def fill(self,content):
# 		self.content=content
# 	def empty(self,content):
# 		self.content=None
# 	def mycontent(self):
# 		print "my cup contains - {}".format(self.content)

# # public
# Yellowcup = Cup()
# print dir(Yellowcup)
# Yellowcup.color="yellow"
# Yellowcup.content="tea"
# Yellowcup.mycontent()


# case II: private
# these variable should be access via a function within the class

# class Cup:
# 	color=None
# 	def __init__(self):
# 		self._content=None
# 	def fill(self,content):
# 		self._content=content
# 	def empty(self,content):
# 		self._content=None
# 	def mycontent(self):
# 		print "my cup contains - {}".format(self._content)

# # public
# Yellowcup = Cup()
# print dir(Yellowcup)
# Yellowcup.color="yellow"
# Yellowcup._content="tea"
# Yellowcup.mycontent()

# protected

class Cup:
	color=None
	def __init__(self):
		self.__content=None
	def fill(self,content):
		self.__content=content
	def empty(self,content):
		self.__content=None
	def mycontent(self):
		print "my cup contains - {}".format(self.__content)

# public
Yellowcup = Cup()
print dir(Yellowcup)
Yellowcup.color="yellow"
Yellowcup.__content="tea"
Yellowcup.mycontent()
#print Yellowcup.__content
# name mangling
#print Cup.__content
'''
Traceback (most recent call last):
  File "nine.py", line 66, in <module>
    print Cup.__content
AttributeError: class Cup has no attribute '__content'
'''
Cup._Cup__content="coffee"
print Cup._Cup__content
print Yellowcup.mycontent()






