#!/usr/bin/python
# namespace issue are taken care by module programming.
# my_add and f.my_add both have their own namespaces and co-exist.

import sys
sys.path.insert(0,'/home/khyaathi/Documents/bit-tuxfux/tuxfux.bitbucket.io/others/batch-01/10-modules/extra')

import first as f

def my_add(a,b):
	''' This is going to add two numbers for us '''
	a = int(a)
	b = int(b)
	return a + b

# Main
if __name__ == '__main__':
	print "addition of two numbers is {}".format(my_add(11,22))
	print "addition of two string is {}".format(f.my_add("linux","rocks"))