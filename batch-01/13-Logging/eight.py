#!/usr/bin/python
# logging.basicConfig
# logging.Formatter
# google.com -> man date
# crontab - linux
# scheduler - windows


import logging
from subprocess import Popen,PIPE
from logging.handlers import SysLogHandler

# create logger
logger = logging.getLogger('Disk Monitor') # logger is a variables.
logger.setLevel(logging.DEBUG)               # filter at logger level.

# create console handler and set level to debug
ch = SysLogHandler(address="/dev/log") # handler
ch.setLevel(logging.DEBUG)   # filter at logging level.

# create formatter
formatter = logging.Formatter(' - %(name)s - %(levelname)s - %(message)s') # layout

# add formatter to ch
ch.setFormatter(formatter) # handler and formatter.

# add ch to logger
logger.addHandler(ch) # logger and handler.

### main

p1 = Popen(['df','-h','/'],stdout=PIPE)
p2 = Popen(['tail','-n','1'],stdin=p1.stdout,stdout=PIPE)
disk_usage = int(p2.communicate()[0].split()[-2].split('%')[0])

if disk_usage < 50: 
	logger.info("The disk is going health at - {}.".format(disk_usage))
elif disk_usage < 75:
	logger.warning("Your disk is getting filled up - {}.".format(disk_usage))
elif disk_usage < 90:
	logger.error("Your disk has started to puke errors - {}".format(disk_usage))
elif disk_usage < 99:
	logger.critical("your applicaiton is down - {}".format(disk_usage))

'''
Improvements:
* I cannot modify the logger name.
* we cannot redirect all logs to the same file.
* we cannot use other handlers and we are restriced to only file handler.

'''