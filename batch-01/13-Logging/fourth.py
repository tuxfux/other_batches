#!/usr/bin/python
# logging.basicConfig
# logging.Formatter
# google.com -> man date
# crontab - linux
# scheduler - windows


import logging
from subprocess import Popen,PIPE

logging.basicConfig(filename='disk_usage.log',filemode='a',level=logging.DEBUG,
					format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
					datefmt='%c')

# df -h / | tail -n 1|awk '{print $5}'|sed -e 's#%# #g'
#disk_usage = int(raw_input("please enter the disk value:"))

p1 = Popen(['df','-h','/'],stdout=PIPE)
p2 = Popen(['tail','-n','1'],stdin=p1.stdout,stdout=PIPE)
disk_usage = int(p2.communicate()[0].split()[-2].split('%')[0])

if disk_usage < 50: 
	logging.info("The disk is going health at - {}.".format(disk_usage))
elif disk_usage < 75:
	logging.warning("Your disk is getting filled up - {}.".format(disk_usage))
elif disk_usage < 90:
	logging.error("Your disk has started to puke errors - {}".format(disk_usage))
elif disk_usage < 99:
	logging.critical("your applicaiton is down - {}".format(disk_usage))

'''
Improvements:
* I cannot modify the logger name.
* we cannot redirect all logs to the same file.
* we cannot use other handlers and we are restriced to only file handler.

'''