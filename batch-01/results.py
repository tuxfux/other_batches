#!/usr/bin/python
#continue : will skip an iteration.
for student in ['abit','madhav','kumar','mahendra','narayan','satyan']:
	if student == 'kumar':
		continue
		#break
		#pass
	print "Results of student - {}".format(student)

## only kumar should get printed
# for student in ['abit','madhav','kumar','mahendra','narayan','satyan']:
# 	if student == 'kumar':
# 		print "Results of student - {}".format(student)
# 		#continue
# 		#break
# 		#pass
	