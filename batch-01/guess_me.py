#!/usr/bin/python
# break: break will take you out of the loop.
# sys.exit : This will exit the code.
# task1: please make sure the total number of trials/guesses is only 3.
# task2: try to randomize your number anything between 1 to 10.

import sys
answer = raw_input("please let me know if you want to continue - y/n:")
if answer == 'n':
	sys.exit()

number = 7 # number in my palm
#test=True


#while test:  # while loop works on truth of a condition
while True:
	my_answer = int(raw_input("please enter the number:"))
	if my_answer > number:
		print "buddy you guessed a larger number."
	elif my_answer < number:
		print "buddy you guessed a smaller number."
	elif my_answer == number:
		print "Congo !!! you guessed the right number."
		break
		#test=False

print "Thank you for playing the game."


'''
# we are actually moving to the loop1

loop1
  loop2
    break
  end2
end1

# This exists the program.

loop1
  loop2
    sys.exit()
  end2
end1


'''

