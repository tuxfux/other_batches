#!/usr/bin/python
import pdb

version=2.0

def my_add(a,b):
	''' This function is for addition of two numbers '''
	print "value of a - {}".format(a)
	print "value of b - {}".format(b)
	return a + b

def my_sub(a,b):
	''' This function is for substraction of two numbers '''
	if a > b:
		return a - b
	else:
		return b - a

def my_div(a,b):
	''' This is for the division of two numbers '''
	return a/b

def my_multi(a,b):
	''' This is for the multiplication of two numbers '''
	return a * b

## MAIN

if __name__ == '__main__':
	print "This is for learning the debugging classes."
	print "The below line will call the functions"
	pdb.set_trace()
	print "Addition of two numbers is -> {}".format(my_add(11,22))
	print "Substraction of two numbers is -> {}".format(my_sub(33,22))
	print "Division of two numbers is -> {}".format(10,2)
	print "Multiplication of two numbers is -> {}".format(10,2)