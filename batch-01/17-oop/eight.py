#!/usr/bin/python
# https://rszalski.github.io/magicmethods/
# polymorphism

class RationalNumber:
    """
    Rational Numbers with support for arthmetic operations.

        >>> a = RationalNumber(1, 2) => a=1/2
        >>> b = RationalNumber(1, 3) => b=1/3
        >>> a + b => a.__add__(b)
        5/6
    """
    def __init__(self, numerator, denominator=1):
        self.n = numerator
        self.d = denominator

    def __add__(self, other):
        if not isinstance(other, RationalNumber):
            other = RationalNumber(other)

        n = self.n * other.d + self.d * other.n # a.n * b.d + a.d * b.n
        d = self.d * other.d                    # a.d * b.d
        return RationalNumber(n, d)             # RationalNumber(5,6) -> self.n,self.d

    def __str__(self):
        return "%s/%s" % (self.n, self.d)

    __repr__ = __str__


## main
# case I
a = RationalNumber(1, 2) # a.n=1,a.d=2
b = RationalNumber(1, 3) # b.n=1,b.d=3
print a + b # a.__add__(b)

# case II
a = 1 # instance of int class
b = 2 # instance of int class
print a + b

# case III
a = RationalNumber(1, 2) # 1/2
b = 3 # RationalNumber(3,1) => 3/1
print a + b # a.__add__(b) # other = RationalNumber(3,1) => 3/1



'''
In [20]: a = 1

In [21]: print type(a)
<type 'int'>

In [22]: isinstance?
Type:        builtin_function_or_method
String form: <built-in function isinstance>
Namespace:   Python builtin
Docstring:
isinstance(object, class-or-type-or-tuple) -> bool

Return whether an object is an instance of a class or of a subclass thereof.
With a type as second argument, return whether that is the object's type.
The form using a tuple, isinstance(x, (A, B, ...)), is a shortcut for
isinstance(x, A) or isinstance(x, B) or ... (etc.).

In [23]: isinstance(a,int)
Out[23]: True

In [24]: isinstance(a,str)
Out[24]: False

In [25]: # a is an object of 'int' class

In [26]: b = "satyan"

In [27]: isinstance(b,str)
Out[27]: True

In [28]: print b,type(b)
satyan <type 'str'>

'''